# 3d

## RCS 800, HV connector to LEAF battery pack
![RCS800](./rcs800/rcs800_32A.jpeg)

Take the GND-pin from two 32A connectors and put them in the printed connector. A 6mm2 cable will fit perfect in these pins.

- [STL files](./rcs800)

For me, best results with printing was:
- rcs800.stl with the oval part up 
- rcs800-housing.stl with the biggest oval part up 
- rcs800-snap.stl with the two holes up 


## Service disconnect plug LEAF ZE0
![leaf-ze0-disconnect](./leaf-ze0-disconnect/ze0-disconnect-2.png)

So, this one was a bit tricky. I wasn't able to find any nice pins as with the Leaf HV connector, so I ended up buying a 9 mm brass pipe. 
There are many ways to get the two, and you can probably find better options than me. Anyway, I left some space within the plug so if you want to add a fuse that should be ok. You can also use a not-so-thick cable as a fuse.

I did two variants;
- Drilled and threaded holes in two pipes, interconnected with a cable.
- Heated up, cooled down and bended the pipe.

The two are shown below.

![leaf-ze0-disconnect](./leaf-ze0-disconnect/ze0-disconnect-1.png)

As for the material, I printed with PLA during testing but ABS is probably a better choice since PLA is a bit fragile and the plug sits very tight. I had to be really careful with the one in PLA, especially when removing the plug.

- [STL files](./leaf-ze0-disconnect)

